## glTF

> The GL Transmission Format (glTF) is an API-neutral runtime asset delivery format.
> glTF bridges the gap between 3D content creation tools and modern 3D applications
> by providing an efficient, extensible, interoperable format for the transmission
> and loading of 3D content.
>
> -- https://github.com/KhronosGroup/glTF

### gltf-codec

The package provides basic types to process JSON and "binary" files.

* No further conversion is performed to keep dependencies to a minimum.
* No validation performed either, assuming you are going to import
  ready-to-use assets.
* ToJSON instances are provided, but not tested.
