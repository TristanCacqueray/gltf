{-# LANGUAGE PartialTypeSignatures #-}

{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Codec.GlTF.Prelude
  ( Size

  , FromJSON(..)
  , ToJSON(..)
  , Generic
  , gParseJSON
  , gToJSON

  , HashMap
  , Object
  , Scientific
  , Text
  , Value
  , Vector

  , withText
  , withObject
  , (.:)
  , (.:?)
  , (.!=)
  ) where

import Data.Aeson (FromJSON(..), ToJSON(..), Value, Object, withText, withObject, (.:), (.:?), (.!=))
import Data.Aeson.Types (Parser, Options(..), defaultOptions, genericParseJSON, genericToJSON)
import Data.List (dropWhileEnd)
import Data.Scientific (Scientific)
import Data.Text (Text)
import Data.Vector (Vector)
import Data.HashMap.Strict (HashMap)
import GHC.Generics (Generic)

type Size = Int

gParseJSON :: _ => Value -> Parser a
gParseJSON = genericParseJSON dropEndTick

gToJSON :: _ => a -> Value
gToJSON = genericToJSON dropEndTick

dropEndTick :: Options
dropEndTick = defaultOptions
  { fieldLabelModifier = dropWhileEnd (== '\'')
  }
