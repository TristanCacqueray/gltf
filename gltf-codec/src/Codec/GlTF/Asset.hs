module Codec.GlTF.Asset where

import Codec.GlTF.Prelude

-- | Metadata about the glTF asset.
data Asset = Asset
  { version    :: Text
  , copyright  :: Maybe Text
  , generator  :: Maybe Text
  , minVersion :: Maybe Text
  , extensions :: Maybe Object
  , extras     :: Maybe Value
  } deriving (Eq, Show, Generic)

instance FromJSON Asset
instance ToJSON Asset
