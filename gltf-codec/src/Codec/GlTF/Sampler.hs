module Codec.GlTF.Sampler
  ( SamplerIx(..)
  , Sampler(..)

  , SamplerWrap(..)
  , pattern CLAMP_TO_EDGE
  , pattern MIRRORED_REPEAT
  , pattern REPEAT

  , SamplerMagFilter(..)
  , pattern MAG_NEAREST
  , pattern MAG_LINEAR

  , SamplerMinFilter(..)
  , pattern MIN_NEAREST
  , pattern MIN_LINEAR
  , pattern MIN_NEAREST_MIPMAP_NEAREST
  , pattern MIN_LINEAR_MIPMAP_NEAREST
  , pattern MIN_NEAREST_MIPMAP_LINEAR
  , pattern MIN_LINEAR_MIPMAP_LINEAR
  ) where

import Codec.GlTF.Prelude

newtype SamplerIx = SamplerIx { unSamplerIx :: Int }
  deriving (Eq, Ord, Show, FromJSON, ToJSON, Generic)

-- | The root object for a glTF Sampler.
data Sampler = Sampler
  { wrapS      :: SamplerWrap
  , wrapT      :: SamplerWrap
  , magFilter  :: Maybe SamplerMagFilter
  , minFilter  :: Maybe SamplerMinFilter
  , name       :: Maybe Text
  , extensions :: Maybe Object
  , extras     :: Maybe Value
  } deriving (Eq, Show, Generic)

instance FromJSON Sampler where
  parseJSON = withObject "Sampler" \o -> do
    wrapS      <- o .:? "wrapS" .!= REPEAT
    wrapT      <- o .:? "wrapT" .!= REPEAT
    magFilter  <- o .:? "magFilter"
    minFilter  <- o .:? "minFilter"
    name       <- o .:? "name"
    extensions <- o .:? "extensions"
    extras     <- o .:? "extras"
    pure Sampler{..}

instance ToJSON Sampler where
  toJSON = gToJSON

-- | Wrapping mode.
--
-- All valid values correspond to WebGL enums.
newtype SamplerWrap = SamplerWrap { unSamplerWrap :: Int }
  deriving (Eq, Ord, Show, FromJSON, ToJSON, Generic)

pattern CLAMP_TO_EDGE :: SamplerWrap
pattern CLAMP_TO_EDGE = SamplerWrap 33071

pattern MIRRORED_REPEAT :: SamplerWrap
pattern MIRRORED_REPEAT = SamplerWrap 33648

pattern REPEAT :: SamplerWrap
pattern REPEAT = SamplerWrap 10497

-- | Magnification filter.
--
-- Valid values correspond to WebGL enums.
newtype SamplerMagFilter = SamplerMagFilter { unSamplerMagFilter :: Int }
  deriving (Eq, Ord, Show, FromJSON, ToJSON, Generic)

pattern MAG_NEAREST :: SamplerMagFilter
pattern MAG_NEAREST = SamplerMagFilter 9728

pattern MAG_LINEAR :: SamplerMagFilter
pattern MAG_LINEAR = SamplerMagFilter 9729

-- | Minification filter.
--
-- All valid values correspond to WebGL enums.
newtype SamplerMinFilter = SamplerMinFilter { unSamplerMinFilter :: Int }
  deriving (Eq, Ord, Show, FromJSON, ToJSON, Generic)

pattern MIN_NEAREST :: SamplerMinFilter
pattern MIN_NEAREST = SamplerMinFilter 9728

pattern MIN_LINEAR :: SamplerMinFilter
pattern MIN_LINEAR = SamplerMinFilter 9729

pattern MIN_NEAREST_MIPMAP_NEAREST :: SamplerMinFilter
pattern MIN_NEAREST_MIPMAP_NEAREST = SamplerMinFilter 9984

pattern MIN_LINEAR_MIPMAP_NEAREST :: SamplerMinFilter
pattern MIN_LINEAR_MIPMAP_NEAREST = SamplerMinFilter 9985

pattern MIN_NEAREST_MIPMAP_LINEAR :: SamplerMinFilter
pattern MIN_NEAREST_MIPMAP_LINEAR = SamplerMinFilter 9986

pattern MIN_LINEAR_MIPMAP_LINEAR :: SamplerMinFilter
pattern MIN_LINEAR_MIPMAP_LINEAR = SamplerMinFilter 9987
